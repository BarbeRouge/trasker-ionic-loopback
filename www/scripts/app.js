'use strict';

angular.module('traskerApp', ['ui.router','ngResource','ngDialog', 'lbServices'])
.config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
        
            // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller  : 'HeaderController'
                    },
                    'content': {
                        templateUrl : 'views/home.html',
                        controller  : 'HomeController'
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                }

            })
        
            // route for the aboutus page
            .state('app.dashboard', {
                url:'dashboard',
                views: {
                    'content@': {
                        templateUrl : 'views/dashboard.html',
                        controller  : 'DashboardController'                  
                    }
                }
            })

            // route for the menu page
            .state('app.mdash', {
                url: 'mdash',
                views: {
                    'content@': {
                        templateUrl : 'views/mdash.html',
                        controller  : 'MdashController'
                    }
                }
            });
    
        $urlRouterProvider.otherwise('/');
    })
;
