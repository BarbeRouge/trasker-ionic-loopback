'use strict';

angular.module('traskerApp')

.controller('MdashController', ['$scope', '$rootScope', function ($scope, $rootScope) {}])

.controller('HomeController', ['$scope', 'Tasks', '$state', function ($scope, Tasks, $state) {
    $scope.message = "Loading ...";
    
    Tasks.find()
    .$promise.then(
        function (response) {
            $scope.tasks = response;
        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        }
    );
    
    $scope.trackIt = function(taskId){
        Tasks.findOne({"filter":{"where":{
            "tracked": "true"
        }}})
        .$promise.then(
            function (response) {
                 Tasks.prototype$updateAttributes({id: response.id},{tracked: false});
                Tasks.findById({id:taskId})
                    .$promise.then(
                        function(subresponse){
                            Tasks.prototype$updateAttributes({id: subresponse.id},{tracked: true}).$promise.then(
                                function (response) {
                                    $state.go('app', {}, {reload: true});
                                },
                                function (response) {
                                    $scope.message = "Error: " + response.status + " " + response.statusText;
                                }
                            );
                        },
                    function(subresponse){
                        $scope.message = "Error: " + subresponse.status + " " + subresponse.statusText;
                    })
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
                Tasks.findById({id:taskId})
                    .$promise.then(
                        function(subresponse){
                            subresponse.tracked = true;
                            Tasks.prototype$updateAttributes({id: subresponse.id},{tracked: true}).$promise.then(
                                function (response) {
                                    $state.go('app', {}, {reload: true});
                                },
                                function (response) {
                                    $scope.message = "Error: " + response.status + " " + response.statusText;
                                }
                            );
                        },
                    function(subresponse){
                        $scope.message = "Error: " + subresponse.status + " " + subresponse.statusText;
                })
            }
        );
    };
    
}])

.controller('DashboardController', ['$scope', function ($scope) {}])

.controller('HeaderController', ['$scope', '$state', '$rootScope', 'ngDialog', 'AuthService', 'Tasks', function ($scope, $state, $rootScope, ngDialog, AuthService, Tasks) {
    
    $scope.loggedIn = false;
    $scope.username = '';
    
    if(AuthService.isAuthenticated()) {
        $scope.loggedIn = true;
        $scope.username = AuthService.getUsername();
    }
        
    $scope.openLogin = function () {
        ngDialog.open({ template: 'views/login.html', scope: $scope, className: 'ngdialog-theme-default', controller:"LoginController" });
    };
    
    $scope.logOut = function() {
       AuthService.logout();
        $scope.loggedIn = false;
        $scope.username = '';
    };
    
    $rootScope.$on('login:Successful', function () {
        $scope.loggedIn = AuthService.isAuthenticated();
        $scope.username = AuthService.getUsername();
    });
        
    $rootScope.$on('registration:Successful', function () {
        $scope.loggedIn = AuthService.isAuthenticated();
        $scope.username = AuthService.getUsername();
    });
    
    $scope.stateis = function(curstate) {
       return $state.is(curstate);  
    };
    
    $scope.stopwatches = [{interval: 0, log: [], diff: 0}];

    Tasks.findOne({"filter":{"where":{
        "tracked": "true"
    }}})
    .$promise.then(
        function (response) {
            $scope.trackedTask = response;
        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        }
    );
    
    $scope.addTask = function () {
        ngDialog.open({ template: 'views/taskcreation.html', scope: $scope, className: 'ngdialog-theme-default', controller:"NewTaskController" });
        //$state.go('app.dashboard');
    };
    
    $scope.updateTaskTime = function(diff){
        Tasks.findOne({"filter":{"where":{"tracked": "true"}}})
        .$promise.then(
            function (response) {
                var tmpValue = Math.round(diff/1000)+parseInt(response.time_spent);
                var tmp = tmpValue.toString();
                 Tasks.prototype$updateAttributes({id: response.id},{time_spent: tmp}).$promise.then(
                        function (subresponse) {
                            $state.go('app', {}, {reload: true});
                        },
                        function (subresponse) {
                            $scope.message = "Error: " + subresponse.status + " " + subresponse.statusText;
                        }
                    );
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
            }
        );
    };
}])
.filter('stopwatchTime', function() {
    return function(input) {
      if (input) {

        var elapsed = input.getTime();
        var hours = parseInt(elapsed / 3600000, 10);
        if(hours < 10) hours = "0" + hours;
        elapsed %= 3600000;
        var mins = parseInt(elapsed / 60000, 10);
        if(mins < 10) mins = "0" + mins;
        elapsed %= 60000;
        var secs = parseInt(elapsed / 1000, 10);
        if(secs < 10) secs = "0" + secs;
        var ms = elapsed % 1000;

        return hours + ':' + mins + ':' + secs;// + ':' + ms;
      }
    };
  })
.directive('bbStopwatch', ['StopwatchFactory', function(StopwatchFactory){
    return {
        restrict: 'EA',
        scope: true,
        link: function(scope, elem, attrs){   
            
            var stopwatchService = new StopwatchFactory(scope[attrs.options]);
            
            scope.startTimer = stopwatchService.startTimer; 
            scope.stopTimer = stopwatchService.stopTimer;
            scope.resetTimer = stopwatchService.resetTimer;
        }
    };
}])
.factory('StopwatchFactory', ['$interval', function($interval){
    
    return function(options){
        var startTime = 0,
            currentTime = null,
            offset = 0,
            interval = null,
            self = this;
        
        if(!options.interval){
            options.interval = 100;
        }

        options.elapsedTime = new Date(0);
        self.running = false;
        
        function pushToLog(lap){
            if(options.log !== undefined){
               options.log.push(lap); 
            }
            options.diff = lap;
        }
         
        self.updateTime = function(){
            currentTime = new Date().getTime();
            var timeElapsed = offset + (currentTime - startTime);
            options.elapsedTime.setTime(timeElapsed);
        };

        self.startTimer = function(){
            if(self.running === false){
                startTime = new Date().getTime();
                interval = $interval(self.updateTime,options.interval);
                self.running = true;
            }
        };

        self.stopTimer = function(){
            if( self.running === false) {
                return;
            }
            self.updateTime();
            offset = offset + currentTime - startTime;
            pushToLog(currentTime - startTime);
            $interval.cancel(interval);  
            self.running = false;
        };

        self.resetTimer = function(){
          startTime = new Date().getTime();
          options.elapsedTime.setTime(0);
          timeElapsed = offset = 0;
        };

        self.cancelTimer = function(){
          $interval.cancel(interval);
        };
        return self;
    };
}])


.controller('LoginController', ['$scope', '$state', 'ngDialog', '$localStorage', 'AuthService', function ($scope, $state, ngDialog, $localStorage, AuthService) {
    
    $scope.loginData = $localStorage.getObject('userinfo','{}');
    
    $scope.doLogin = function() {
        if($scope.rememberMe)
           $localStorage.storeObject('userinfo',$scope.loginData);

        AuthService.login($scope.loginData);

        ngDialog.close();
    };
            
    $scope.openRegister = function () {
        ngDialog.open({ template: 'views/register.html', scope: $scope, className: 'ngdialog-theme-default', controller:"RegisterController" });
    };
    
}])
.controller('NewTaskController', ['$scope', '$state', 'ngDialog', 'Tasks', function ($scope, $state, ngDialog, Tasks) {
    
    $scope.mytask = {};
    $scope.doNewTask = function() {
        
        Tasks.create($scope.mytask).$promise.then(
            function (response) {
                $state.go('app', {}, {reload: true});
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
            }
        );

        $scope.mytask = {};
        ngDialog.close();
        
    };
}])
.controller('RegisterController', ['$scope', 'ngDialog', '$localStorage', 'AuthService', function ($scope, ngDialog, $localStorage, AuthService) {
    
    $scope.register={};
    $scope.loginData={};
    
    $scope.doRegister = function() {

        AuthService.register($scope.registration);
        
        ngDialog.close();

    };
}])
;